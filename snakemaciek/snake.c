#include <stdio.h>
#include<conio.h>
#include<ctype.h>
#include<time.h>
#include<Windows.h>
#include <stdlib.h>


#define SZERPLANSZY 31
#define WYSPLANSZY 21
#define DLUGOSCPOCZ 4

#define GORA 72
#define DOL 80
#define LEWA 75
#define PRAWA 77
int punkty;

typedef struct fifo {

	int wspolrzednekonca;
	struct fifo *nast;

} kolejka;//struktura kolejki first in first out
//obsluga kolejki
int odczyt(kolejka* a) {

	return a->wspolrzednekonca;

}
kolejka * usun(kolejka *a) 
{
	kolejka *temp;
		temp = a;
		a = a->nast; 
		free(temp);
	return a;
}
void wstaw(kolejka *a, int co) {
	while (a->nast != NULL)
	{
		a = a->nast;
	}

	kolejka *nowy = (kolejka*)malloc(sizeof(kolejka));

	nowy->wspolrzednekonca = co;
	nowy->nast = NULL;
	a->nast = nowy; // podlaczamy nowa do starej
}

void kursordoXY(int x, int y)
{
	HANDLE a;
	COORD b;
	fflush(stdout);
	b.X = x;
	b.Y = y;
	a = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(a, b);
}
void powitanie()
{
	printf("			Witaj w grze snake!\n\n");
	printf("________________________________________________________________________________\n");
	printf("				Zasady:\n\n");
	printf("			->sterujemy strzalkami\n\n");
	printf("		->celem gry jest zbieranie jak najwiecej 'o'\n\n");
	printf("		  ->gdy natrafisz na '#' przegrywasz\n\n");
	printf("		 ->gdy ugryziesz siebie - przegrywasz\n\n\n\n\n");
	printf("________________________________________________________________________________\n");
	system("pause");
}
void wyswietlanie(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	int i, j;
	for (j = 0; j < WYSPLANSZY; j++)
	{
		for (i = 0; i < SZERPLANSZY; i++)
		{
			printf("%c", tab_gry[i][j]);
			if (i == SZERPLANSZY - 1)
				printf("\n");
		}
	}
}
void ramka(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	int i;
	for (i = 0; i < SZERPLANSZY; i++)
	{
		tab_gry[i][0] = '#';
		tab_gry[i][WYSPLANSZY - 1] = '#';
	}
	for (i = 0; i < WYSPLANSZY; i++)
	{
		tab_gry[0][i] = '#';
		tab_gry[SZERPLANSZY - 1][i] = '#';
	}
}
void pole_minowe(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	{
		int i,j;
		ramka(tab_gry);
		for (i = 0; i < SZERPLANSZY; i++)
		{
			for (j = 0; j < WYSPLANSZY; j++)
			{
				if ((i == (rand() % (SZERPLANSZY - 2)) + 1)) tab_gry[i][j] = '#';

			}
		}
	}
}
void labirynt(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	{
		int i, j,p,k,z=0;
		ramka(tab_gry);
		for (i = 3; i < SZERPLANSZY-3; i++)
		{
			p = (rand() % 3);
			k = (rand() % 5);
			for (j = 0; j < WYSPLANSZY; j++)
			{
				if ((i>z)&&((i<15)||(i>18)))
				{
					if ((j < WYSPLANSZY - (8+ k)) && (p % 2 == 0)) tab_gry[i][j] = '#';
					if ((j > (7+ k)) && (p % 2 == 1)) tab_gry[i][j] = '#';
					if((j == WYSPLANSZY-1)) z = i + 3;
					
				}
			}
		}
	}
}
void jedzenie(char tab_gry[SZERPLANSZY][WYSPLANSZY], int x, int y)
{
	int i, j;
	i = (rand() % (SZERPLANSZY-2)) + 1;
	j = (rand() % (WYSPLANSZY-2)) + 1;
	while ((tab_gry[i][j] == '*')|| tab_gry[i][j] == '#')
	{
		i = (rand() % SZERPLANSZY - 2) + 1;
		j = (rand() % WYSPLANSZY - 2) + 1;
	}
	tab_gry[i][j] = 'o';
	punkty++;
}
int niewcisniete(char tab_gry[SZERPLANSZY][WYSPLANSZY], int *aktualny_x, int *aktualny_y, int*klawisz)
{
	if (*klawisz == PRAWA)
	{
		(*aktualny_x) = (*aktualny_x) + 1;
		if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
		else tab_gry[*aktualny_x][*aktualny_y] = '*';	
	}
	else if (*klawisz == LEWA)
	{
		(*aktualny_x) = (*aktualny_x) - 1;
		if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
		tab_gry[*aktualny_x][*aktualny_y] = '*';
	}
	else if (*klawisz == GORA)
	{
		(*aktualny_y) = (*aktualny_y) - 1;
		if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
		tab_gry[*aktualny_x][*aktualny_y] = '*';
	}
	else if (*klawisz == DOL)
	{
		(*aktualny_y) = (*aktualny_y) + 1;
		if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
		tab_gry[*aktualny_x][*aktualny_y] = '*';
	}
	return 0;
}
int przegrana(char tab_gry[SZERPLANSZY][WYSPLANSZY], int *aktualny_x, int *aktualny_y)
{
	if (tab_gry[*aktualny_x][*aktualny_y] == 'o')
	{
		jedzenie(tab_gry, aktualny_x, aktualny_y);
		return 0;
	}
	else if (tab_gry[*aktualny_x][*aktualny_y] == '#') return 1;
	else if (tab_gry[*aktualny_x][*aktualny_y] == '*') return 1;
	else return 0;
	return 0;
}
int strza�ki(char tab_gry[SZERPLANSZY][WYSPLANSZY],int *aktualny_x, int *aktualny_y,int*klawisz)
{
	char kierunek;
	kierunek = *klawisz;
	if (_kbhit())
	{
		*klawisz = _getch();
		switch (*klawisz)
		{
		case 224:
			*klawisz = _getch();
			if ((*klawisz == PRAWA)&&(kierunek != LEWA))
			{				
				(*aktualny_x) = (*aktualny_x) + 1;
				if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
				else tab_gry[*aktualny_x][*aktualny_y] = '*';

			}
			else if ((*klawisz == LEWA) && (kierunek != PRAWA))
			{
				(*aktualny_x) = (*aktualny_x) - 1;
				if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
				else tab_gry[*aktualny_x][*aktualny_y] = '*';
			}
			else if ((*klawisz == GORA) && (kierunek != DOL))
			{
				
				(*aktualny_y) = (*aktualny_y) - 1;
				if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
				else tab_gry[*aktualny_x][*aktualny_y] = '*';
			}
			else if ((*klawisz == DOL) && (kierunek != GORA))
			{
				(*aktualny_y) = (*aktualny_y) + 1;
				if (przegrana(tab_gry, aktualny_x, aktualny_y)) return 1;
				else tab_gry[*aktualny_x][*aktualny_y] = '*';
			}
			else
			{
				*klawisz = kierunek;
				if (niewcisniete(tab_gry, aktualny_x, aktualny_y, klawisz)) return 1;
			}
			break;
		default:
			*klawisz = kierunek;
			if (niewcisniete(tab_gry, aktualny_x, aktualny_y, klawisz)) return 1;
			break;
		}
	}
	else
	{
		if (niewcisniete(tab_gry, aktualny_x, aktualny_y, klawisz)) return 1;
	}
return 0;
}
void zapisz_do_pliku(int punkty) {
	char imie[30];
	int i;
	FILE *info;
	info = fopen("info.txt", "a+");
	system("cls");
	printf("wpisz swoje imie: \n");
	scanf("%s", &imie);
	fprintf(info, "Imie gracza :%s\n", imie);
	time_t mytime;
	mytime = time(NULL);
	fprintf(info, "data: %s", ctime(&mytime));
	fprintf(info, "wynik: :%d\n", punkty);
	for (i = 0; i <= 50; i++)
		fprintf(info, "%c", '_');
	fprintf(info, "\n");
	fclose(info);
	printf("zapisano!");
	_getch();
}
void otworz_plik()
{
	FILE *info;
	char c;
		info = fopen("info.txt", "r");
		do {
			putchar(c = getc(info));
		} while (c != EOF);
		system("pause");
		fclose(info);
}
int wlasna_plansza(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	{
		FILE *plansza;
		char c;
		char nazwa[20];
		int i, j;
		printf("			plansze dostepne w katalogu: \n");
		system("dir *.txt");
		printf("\n");
		printf("	wpisz nazwe swojej planszy: ");
		scanf("%s", &nazwa);
		plansza = fopen(nazwa, "r");
		if (plansza == NULL)
		{
			printf("		nie znaleziono planszy\n");
			printf("________________________________________________________________________________\n");
			_getch();
			return 5;
		}
		else 
		{
			for (j = 0; j < WYSPLANSZY; j++)
			{
				for (i = 0; i < SZERPLANSZY; i++)
				{
					c = getc(plansza);
					if (c == '#') tab_gry[i][j] = c;
				}
			}
			printf("			wczytano!\n");
			printf("________________________________________________________________________________\n");
			fclose(plansza);
			_getch();
			return 4;
		}
	}
}
void wpisywanie(int *i)
{
	int retVal;
	char ch;
	retVal = scanf("%d", &*i);
	while ((retVal != 1))
	{
		printf("Niepoprawna wartosc!\n");
		printf("wpisz jezcze raz:");
		while ((ch = getchar()) != '\n' && ch != EOF)
		{
		}
		retVal = scanf("%d", &*i);
	}
}
void zerowanie(kolejka *wskx,kolejka* *wsky, char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	int i, j;
	while (wskx != NULL)
	{
		(kolejka*)wskx = usun(wskx);
		(kolejka*)wsky = usun(wsky);
	}
	for (i = 0; i < SZERPLANSZY; i++)
	{
		for (j = 0; j < WYSPLANSZY; j++)
		{
			tab_gry[i][j] = ' ';
		}
	}
}
void generator_plansz()
{
	int i, j;
	int x = 0;
	int y = 0;
	char tab_gry[SZERPLANSZY][WYSPLANSZY] = { 0 };
	char c;
	char klawisz;
	char bufor[20];
	FILE* plik,*do_zapisu;
	printf("			Witaj w grze generatorze plansz!\n\n");
	printf("________________________________________________________________________________\n");
	printf("				Zasady:\n\n");
	printf("			->sterujemy strzalkami\n\n");
	printf("		   ->spacja tworzymy  '#'\n\n");
	printf("		  ->'backspace' usuwamy '#'\n\n");
	printf("		 ->'esc' wychodzimy z generatora, milej zabawy :)\n\n\n\n\n");
	printf("________________________________________________________________________________\n");
	_getch();
	system("cls");
	plik = fopen("pustaplansza.txt","r");
	if (plik == NULL)
	{
		printf("		wystapil blas!\n");
		printf("________________________________________________________________________________\n");
		_getch();
	}
	else
	{
		for (j = 0; j < WYSPLANSZY; j++)
		{
			for (i = 0; i < SZERPLANSZY; i++)
			{
				c = getc(plik);
				if (c == '#') tab_gry[i][j] = c;
			}
		}
		fclose(plik);
		for (;;)
		{
			kursordoXY(0, 0);
			wyswietlanie(tab_gry);
			if (_kbhit())
			{
				klawisz = _getch();
				//printf("%d", klawisz);
				//system("pause");
				switch (klawisz)
				{
				case (-32):
					klawisz = _getch();
					if (klawisz == PRAWA)
					{
						x++;
					}
					else if (klawisz == LEWA)
					{
						x--;
					}
					else if (klawisz == GORA)
					{
						y--;
					}
					else if (klawisz == DOL)
					{
						y++;
					}
					break;
				case (32):
					tab_gry[x][y] = '#';
					break;
				case(8):
					tab_gry[x][y] = ' ';
					break;
				}
			}
			else
			{
				c = tab_gry[x][y];
				tab_gry[x][y]=219;
				kursordoXY(0, 0);
				wyswietlanie(tab_gry);
				Sleep(400);
				tab_gry[x][y] = c;
			}
			if (klawisz == 27)
			{
				printf("czy zapisac plansze?[t/n]\n");
				klawisz = _getch();
				if (klawisz == 't')
				{
					printf("podaj nazwe planszy: ");
					scanf("%s", &bufor);
					do_zapisu = fopen(bufor, "w");
					for (j = 0; j < WYSPLANSZY; j++)
					{
						for (i = 0; i < SZERPLANSZY; i++)
						{
							putc(tab_gry[i][j],do_zapisu);
						}
						//putc('\n',do_zapisu);
					}
					fclose(do_zapisu);
				}
				system("cls");
				break;
			}
		}
	}
}
int menu()
{
	for (;;)
	{
		int i;
		system("cls");
		printf("\n");
		printf("			  Przegrales!\n\n");
		printf("________________________________________________________________________________\n");
		printf("			Twoj wynik to: %d\n\n", punkty);
		printf("			[1]zapisz wynik\n\n");
		printf("		  [2]zobacz poprzednie wyniki\n\n");
		printf("		    [3]zagraj jeszcze raz\n\n");
		printf("			[0]wyjdz z programu\n\n\n");
		printf("________________________________________________________________________________\n");
		wpisywanie(&i);
		switch (i)
		{
		case 0:
			return 0;
		case 1:
			zapisz_do_pliku(punkty);
			break;
		case 2:
			otworz_plik();
			break;
		case 3:
			return 1;
		default:
			break;
		}
	}
}
int wybor_trybu(char tab_gry[SZERPLANSZY][WYSPLANSZY])
{
	int i=0;
	while ((i != 1) && (i != 2)&&(i !=3)&&(i != 4) && (i != 5))
	{
		system("cls");
		printf("________________________________________________________________________________\n");
		printf("			Jaki tryb gry wybierasz?\n\n");
		printf("				[1]Klasyczny\n\n");
		printf("				[2]pole minowe\n\n");
		printf("				[3]labirynt\n\n");
		printf("			[4]wczytaj wlasny poziom\n\n");
		printf("			[5]stworz wlasny poziom\n\n");
		printf("________________________________________________________________________________");
		wpisywanie(&i);
		switch (i)
		{
		case 1:
			ramka(tab_gry);
			break;
		case 2:
			pole_minowe(tab_gry);
			break;
		case 3:
			labirynt(tab_gry);
			break;
		case 4:
			i=wlasna_plansza(tab_gry);
			break;
		case 5:
			system("cls");
			generator_plansz();
			return 1;
			break;
		default:
			printf("nie ma takiej opcji\n");
			_getch();
			break;
		}
		return 0;
	}
	system("cls");
}
void opoznienie(int dlugosc)
{
	int czas;
	czas = 100 * (dlugosc / 4);
	if ((500 - czas) >= 0) Sleep(500 - czas);
	
}
int wybor_trudnosci()
{
	int i=0;
	printf("wybor trudnosci:\n");
	printf("[1]latwy\n");
	printf("[2]trudny\n");
	while((i != 1)&&(i !=2))
	{
	wpisywanie(&i);
	switch (i)
	{
	case 1:
		system("cls");
		return 1;
	case 2:
		system("cls");
		return 0;
	default:
		printf("nie ma takiej opcji\n");
	}

	}
}
int main()
{
	char tab_gry[SZERPLANSZY][WYSPLANSZY] = { 0 };
	int *klawisz;
	int ogon, trudnosc;
	int *aktualny_x;
	int *aktualny_y;
	kolejka *wskx;
	kolejka *wsky;
	srand(time(NULL));

	powitanie();

poczatek:
	wskx = (kolejka*)malloc(sizeof(kolejka));
	wsky = (kolejka*)malloc(sizeof(kolejka));
	ogon = DLUGOSCPOCZ;
	punkty = 0;
	wsky->nast = NULL;
	wskx->nast = NULL;
	aktualny_x = (int*)(SZERPLANSZY/2);
	aktualny_y = (int*)(WYSPLANSZY/2);
	klawisz = (int*)PRAWA;
	wskx->wspolrzednekonca = (int)aktualny_x;
	wsky->wspolrzednekonca = (int)aktualny_y;
	while(wybor_trybu(tab_gry));
	system("cls");
	trudnosc = wybor_trudnosci();
	jedzenie(tab_gry, aktualny_x, aktualny_y);
	punkty = 0;
	for (;;)
	{	
		printf("\nliczba punktow; %d\n\n", punkty);
		if (punkty >= ogon-2*DLUGOSCPOCZ+2)
		{
			ogon++;
			wstaw(wskx, aktualny_x);
			wstaw(wsky, aktualny_y);
		}
		wyswietlanie(tab_gry);
		if (trudnosc) opoznienie((ogon));
		else opoznienie(16);
		if (strza�ki(tab_gry, &aktualny_x, &aktualny_y, &klawisz)) break;
		wstaw(wskx, aktualny_x);
		wstaw(wsky, aktualny_y);
		tab_gry[odczyt(wskx)][odczyt(wsky)] = ' ';	
		wskx = usun(wskx);
		wsky = usun(wsky);//usuwa koniec weza
		kursordoXY(0, 0);								//przesuwa kursor na poczatek
	}
	zerowanie(wskx, wsky, tab_gry);
	if (menu() == 1) goto poczatek;
	return 0;
}
